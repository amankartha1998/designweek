using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateObstacle : MonoBehaviour
{
    public GameObject Prefab;
    int orientationThreshold;

    [SerializeField] private Transform obstaclePos;
    [SerializeField] private float offset;

// Start is called before the first frame update
void Start()
    {
        orientationThreshold = 10;
    }

    // Update is called once per frame
    void Update()
    {
        //print(orientationThreshold);
    }

    public void instantiate()
    {
        float yPos = 0;
        int orientation = Random.Range(0, 21);
        int sizeVariants = Random.Range(0, 2);

        //Vector3 objectScale = Prefab.transform.localScale;

        //print("Size:" + sizeVariants);
        //print("Orient" + orientation);

        
        if(sizeVariants == 0)
        {

            Prefab.transform.localScale = new Vector3(1, (float)4.629105, 1);

            if (orientation <= orientationThreshold)
            {
                yPos = obstaclePos.position.y - offset;
                orientationThreshold--;

            }
            else if (orientation > orientationThreshold)
            {
                yPos = obstaclePos.position.y + offset;
                orientationThreshold++;
            }
        } else if (sizeVariants == 1)
        {

            Prefab.transform.localScale = new Vector3(1, (float)4.629105 / 2, 1);

            if (orientation <= orientationThreshold)
            {
                yPos = obstaclePos.position.y - offset - (float)1.2;
                orientationThreshold--;

            }
            else if (orientation > orientationThreshold)
            {
                yPos = obstaclePos.position.y + offset + (float)1.2;
                orientationThreshold++;
            }
        }
        
       
        Instantiate(Prefab, new Vector3(obstaclePos.position.x,yPos,obstaclePos.position.z), Quaternion.identity);

       
       
    }
}
