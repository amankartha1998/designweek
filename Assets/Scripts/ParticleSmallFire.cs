using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSmallFire : MonoBehaviour ,IVent
{
    public float loudness = 0.0f;

    float lerpMinimum = 0.0f;
    float lerpMaximum = 0.2f;

    float lerpValue;

    [SerializeField] private float Strength = 2f;
    bool addCount = true;

    [SerializeField] private ParticleWin bigFire;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       

        ParticleSystem ps = GetComponent<ParticleSystem>();
        var main = ps.main;

        main.startLifetime = 0;

        //Testing purposes, imitates loudness (volume)
        // if (Input.GetKeyDown(KeyCode.UpArrow))
        // {
        //     loudness = loudness + 0.1f;
        // }
        //
        // if (Input.GetKeyDown(KeyCode.DownArrow))
        // {
        //     loudness = loudness - 0.1f;
        // }

        lerpValue = Mathf.Lerp(lerpMinimum, lerpMaximum, loudness);
        main.startLifetime = lerpValue;

        if (loudness < 0.4f)
        {
            loudness = Mathf.Max (0, loudness - 0.1f * Time.deltaTime);
        }

        if (loudness >= 0.9 && addCount)
        {
            bigFire.counter++;
            addCount = false;
        }

        if (loudness > 1.5f)
        {
            bigFire.counter--;
            addCount = true;
            loudness = 0.0f;
        }

    }

    public void ApplyForce(float windStrength, Transform playerPos)
    {
        if (Vector3.Distance(playerPos.position, this.transform.position) < 3f)
        {
            loudness = loudness + (windStrength * MicInput.MicLoudness * Strength);
        }
    }
}
