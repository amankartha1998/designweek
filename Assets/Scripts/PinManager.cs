using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinManager : MonoBehaviour
{
    public GameObject[] pins = new GameObject[10];
    public GameObject pinPrefab;
    public Vector3[] pinLocation = new Vector3[10];
    public int score = 0;


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            pins[i] = GameObject.Find("Pin" + (i + 1));
        }
        for (int i = 0; i < 10; i++)
        {
            pinLocation[i] = pins[i].transform.position;
        }
        for(int i = 0; i < 10; i++)
        {
            pins[i] = Instantiate(pinPrefab, pinLocation[i], new Quaternion(0, 0, 0, 0));
        }
        /*
         * Finds all the pins locations in the first for loop
         * Saves their position to a new variable in the 2nd for loop
         * finally instantiates the pins at their positions
         */
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetPins()
    {
        for (int i = 0; i < 10; i++)
        {
            Destroy(pins[i]);
            pins[i] = Instantiate(pinPrefab, pinLocation[i], new Quaternion(0, 0, 0, 0));
            //Destroys then reinstantiates all pins
        }
    }
}
