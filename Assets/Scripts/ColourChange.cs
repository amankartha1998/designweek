using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourChange : MonoBehaviour
{
    Color colorStart = new Color(0.0f, 0.0f, 0.0f);
    Color colorEnd = new Color(1.0f, 0.0f, 0.0f);

    Renderer rend;

    [SerializeField]
    private  ParticleSmallFire fireScript;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();

        GetComponent<Renderer>().material = Instantiate<Material>(GetComponent<Renderer>().material);
    }

    // Update is called once per frame
    void Update()
    {
        

        if (fireScript.loudness < 0.4f)
        {
            rend.material.color = Color.Lerp(colorStart, colorEnd, fireScript.loudness);
        }

        if (fireScript.loudness >= 0.9)
        {
            rend.material.color = new Color(1.0f, 0.0f, 0.0f);
        }

        if (fireScript.loudness > 1.5f)
        {
            rend.material.color = colorStart;
        }
    }
}
