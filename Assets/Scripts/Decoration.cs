using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class Decoration : MonoBehaviour,IVent
{
    Rigidbody rb;

    public void ApplyForce(float windStrength, Transform playerPos)
    {
        rb.AddForce(-playerPos.transform.forward * windStrength * MicInput.MicLoudness, ForceMode.Impulse);
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
