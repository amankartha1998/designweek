using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{

    public float moveSpeed = 2f;
    [SerializeField] private Transform endPos;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject,8.5f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * (moveSpeed * Time.deltaTime));

        // if(transform.position.x <= endPos.position.x)
        // {
        //     Destroy(gameObject);
        // }
    }
}
