using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatherMovement : MonoBehaviour
{
    public float moveSpeed = 2f;
    public Rigidbody physics;
    public GameObject director;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetKey(KeyCode.Q))
        // {
        //     physics.AddForce(new Vector3(0, moveSpeed, 0), ForceMode.Force);
        // }
        //
        // if (Input.GetKey(KeyCode.S))
        // {
        //     //physics.AddForce(new Vector3(0, -moveSpeed, 0), ForceMode.Force);
        // }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Environment")
        {
            director.GetComponent<GameEnd>().Reset();
        }
    }
}
