using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleWin : MonoBehaviour
{
    public float counter = 0;

    private ParticleSystem ps;

    private bool hasBeenLit = false;
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        ps.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        

        

      

        if (counter >= 4 && !hasBeenLit)
        {
            ps.Play();
            //TurnOffSmallFires();
            hasBeenLit = true;
        }
      

      
    }

    private void TurnOffSmallFires()
    {
        List<ParticleSmallFire> fireScripts = GameObject.FindObjectsOfType<ParticleSmallFire>().ToList();
        foreach (var fire in fireScripts)
        {
            fire.gameObject.GetComponent<ParticleSystem>().Stop();
        }
    }
}
