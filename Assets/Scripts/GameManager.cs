using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject ball;
    public GameObject ballPrefab;
    public int forceAmt;

    public GameObject pinManager;
    public Vector3 ballStartingPos;


    bool shotBall = false;
    // Start is called before the first frame update
    void Start()
    {
        ballStartingPos = ball.transform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetMouseButtonDown(0) && !shotBall) //If the ball hasnt yet been shot, this shoots the ball
        // {
        //     ball.GetComponent<Rigidbody>().AddForce(Vector3.forward * forceAmt, ForceMode.Impulse);
        //     shotBall = true;
        // }
        //
        // if (Input.GetKeyDown(KeyCode.R))
        // {
        //     ResetLane();
        // }

    }

    private void FixedUpdate()
    {
        // if (Input.GetKey(KeyCode.A) && !shotBall && ball.transform.position.x > -6.5f) //Limits how far the ball can go to the left
        // {
        //     ball.transform.position += new Vector3(-0.1f, 0, 0);
        // }
        // if (Input.GetKey(KeyCode.D) && !shotBall && ball.transform.position.x < 6.5f) //Limits how far the ball can go to the right
        // {
        //     ball.transform.position += new Vector3(0.1f, 0, 0);
        // }
    }

    public void ResetLane()
    {
        /*
         * Destroys the ball
         * Resets the shot bool
         * Instantiates the new ball at the starting position
         * tells the pins to delete themselves and re-instantiate themselves at their starting pos
         */
        Destroy(ball);
        shotBall = false;
        ball = Instantiate(ballPrefab, ballStartingPos, new Quaternion(0, 0, 0, 0));
        pinManager.SendMessage("ResetPins");
    }
}
