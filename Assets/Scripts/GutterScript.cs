using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GutterScript : MonoBehaviour
{
    GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            gm.SendMessage("ResetLane");
        }
        if(other.gameObject.tag == "Pin")
        {
            Destroy(other.gameObject);
        }
    }
}
