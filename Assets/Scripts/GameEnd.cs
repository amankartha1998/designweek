using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnd : MonoBehaviour
{
    public GameObject feather;
    Rigidbody featherPhys;
    CreateObstacle obsGen;

    [SerializeField] private Transform StartPos;
    
    bool gameDone = false;

    // Start is called before the first frame update
    void Start()
    {
        featherPhys = feather.GetComponent<Rigidbody>();
        featherPhys.constraints = RigidbodyConstraints.FreezeAll;
        obsGen = this.GetComponent<CreateObstacle>();
        //Reset();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameDone == true)
        {
            Restart();
        }
    }

    public void Reset()
    {
        //print("TEST MESSAGE");

        gameDone = true;

        DestroyThis[] targets = GameObject.FindObjectsOfType<DestroyThis>();

        for(int i = 0; i<= targets.Length - 1; i++)
        {
            Destroy(targets[i].gameObject);
        }

        feather.transform.position = StartPos.position;
        featherPhys.constraints = RigidbodyConstraints.FreezePosition;

        obsGen.CancelInvoke("instantiate");
    }

    public void Restart()
    {
        gameDone = false;
        featherPhys.constraints = RigidbodyConstraints.None;
        obsGen.InvokeRepeating("instantiate", 0f, 2.5f);
    }
}
