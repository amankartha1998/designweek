using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoiseBar : MonoBehaviour
{
    public Slider slider;
    private float lastValue;

    public float smooth = 5f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        

        slider.value =  Mathf.Lerp(lastValue , MicInput.MicLoudness,smooth * Time.deltaTime);
        lastValue = slider.value;
    }
}
