using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : MonoBehaviour, IVent
{
    Rigidbody body;
    public float force;

    [SerializeField] 
    GameObject initPosition;
    private void Start()
    {
        body = gameObject.GetComponent<Rigidbody>();
    }
    private void Update()
    {
        // if (Input.GetKeyDown(KeyCode.W))
        // {
        //     body.AddForce(0, 0, force, ForceMode.Impulse);
        // }
        // if (Input.GetKeyDown(KeyCode.S))
        // {
        //     body.AddForce(0, 0, -1*force, ForceMode.Impulse);
        // }
        // if (Input.GetKeyDown(KeyCode.D))
        // {
        //     body.AddForce(force, 0, 0, ForceMode.Impulse);
        // }
        // if (Input.GetKeyDown(KeyCode.A))
        // {
        //     body.AddForce(-1*force, 0, 0, ForceMode.Impulse);
        // }
        // if ((body.velocity.magnitude > 0.01) && (body.velocity.normalized.z != 0))
        // {
        //     transform.LookAt(transform.position + body.velocity);
        // }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Rock"))
        {
            gameObject.transform.position = initPosition.transform.position;
            body.velocity = Vector3.zero;
        }
    }

    public void ApplyForce(float windStrength,Transform playerPos)
    {
        body.AddForce(-playerPos.forward * windStrength * MicInput.MicLoudness * 0.1f,ForceMode.Impulse);
    }
}
