using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private float speed = 0.1f;
    [SerializeField] private GameObject floor;
    [SerializeField] private float Offset = 2f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.rotation = Camera.main.transform.rotation;
        this.transform.position +=   Camera.main.transform.right * (speed * Input.GetAxis("Horizontal"))+ 
                                     (Camera.main.transform.forward * (speed *Input.GetAxis("Vertical")));

        transform.position = new Vector3(this.transform.position.x,
            floor.transform.position.y + Offset,
            this.transform.position.z); 
    }
}
