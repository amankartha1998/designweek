using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour, IVent
{
    // Start is called before the first frame update
    private Rigidbody rb;

    [SerializeField] private float power = 5f;
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyForce(float windStrength, Transform playerPos)
    {
        rb.AddForce(-playerPos.forward * windStrength * MicInput.MicLoudness * power,ForceMode.Impulse);
    }
}
