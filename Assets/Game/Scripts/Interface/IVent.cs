using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IVent
{
   public void ApplyForce(float windStrength,Transform playerPos);
}
