using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyForce : MonoBehaviour, IVent
{

    [SerializeField] private FeatherMovement fm;

    public float power = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyForce(float windStrength, Transform playerPos)
    {
        fm.physics.AddForce(Vector3.up*windStrength * power * MicInput.MicLoudness,ForceMode.Impulse);
    }
}
