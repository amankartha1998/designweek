using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vent : MonoBehaviour , IVent
{
    [SerializeField] private List<GameObject> ObjectsToMove;
    // Start is called before the first frame update
    private List<Rigidbody> rbList = new List<Rigidbody>();
    void Start()
    {
        foreach (GameObject gameObject in ObjectsToMove)
        {
            rbList.Add((gameObject.GetComponent<Rigidbody>()));
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyForce(float windStrength,Transform playerPos)
    {
        foreach (Rigidbody rb in rbList)
        {
            rb.AddForce(Vector3.up * MicInput.MicLoudness * windStrength, ForceMode.Impulse);
        }
    }
}
