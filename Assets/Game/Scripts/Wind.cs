using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Wind : MonoBehaviour
{
   [SerializeField] private float WindStrength = 1 ;
   [SerializeField] private Transform player;
   public void OnTriggerStay(Collider other)
   {
     
      if (other.TryGetComponent<IVent>(out IVent vent))
      {
         Debug.Log(other.name);
         vent.ApplyForce(WindStrength,player);
      }
      
      
   }
}
