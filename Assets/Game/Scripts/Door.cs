using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Door : MonoBehaviour,IVent
{
    // Start is called before the first frame update
    private Rigidbody rb;
    [SerializeField] private Collider Blocker;
    [SerializeField] private Collider PlayerCollider;
    [SerializeField] private float rotationThreshold;
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        Physics.IgnoreCollision(this.GetComponent<Collider>(), PlayerCollider);
        Physics.IgnoreCollision(this.GetComponent<Collider>(), Blocker);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(math.abs(transform.rotation.eulerAngles.y) + " " + math.abs( rotationThreshold));
        if ( math.abs(transform.rotation.y) > math.abs( rotationThreshold))
        {
            rb.detectCollisions = false;
            rb.velocity = Vector3.zero;
           
            Blocker.gameObject.SetActive(false);
        }
    }

    public void ApplyForce(float windStrength, Transform playerPos)
    {
        //Debug.Log(playerPos.transform.forward * MicInput.MicLoudness * windStrength);
        rb.AddForce(-playerPos.transform.forward * MicInput.MicLoudness * windStrength * 2,ForceMode.Impulse);
        
    }
}
